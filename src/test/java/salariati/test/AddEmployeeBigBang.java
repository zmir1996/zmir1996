package salariati.test;

import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AddEmployeeBigBang {
    @Test
    public void unitTestValidator() throws EmployeeException {
        EmployeeValidator employeeValidator = new EmployeeValidator();
        Employee newEmployee = new Employee("Vasile", "ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
    }

    @Test
    public void unitTestRepo() throws EmployeeException, IOException {
        File tempFile = File.createTempFile("employee", "txt");

        EmployeeRepositoryInterface repo = new EmployeeImpl(tempFile.getAbsolutePath());
        Employee validEmpl = new Employee("gelu", "george", "1951207060028", DidacticFunction.ASISTENT, "1200");
        assertTrue(repo.addEmployee(validEmpl));

        tempFile.delete();
    }

    @Test
    public void unitTestController() throws EmployeeException, IOException {
        EmployeeMock employeeRepository = new EmployeeMock();
        EmployeeController controller = new EmployeeController(employeeRepository);

        Employee newEmployee = new Employee("Vasile", "ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
    }

    @Test
    public void IntegrationTest() throws EmployeeException, IOException {
        File tempFile = File.createTempFile("employee", "txt");

        EmployeeRepositoryInterface repo = new EmployeeImpl(tempFile.getAbsolutePath());
        EmployeeController controller = new EmployeeController(repo);

        Employee newEmployee = new Employee("Vasile", "ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        controller.addEmployee(newEmployee);
        assertEquals(1, controller.getEmployeesList().size());

        tempFile.delete();
    }
}