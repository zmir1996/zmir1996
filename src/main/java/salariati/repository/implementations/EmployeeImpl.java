package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	
	private String employeeDBFile = "employeeDB/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	public EmployeeImpl(String file) {
		employeeDBFile = file;
	}

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}


	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) throws EmployeeException {
		List<Employee> employeeList = getEmployeeList();
		Comparator<Employee> comparator;

		if(criteria.equals("salaryDesc")) {
			comparator = new Comparator<Employee>() {
				@Override
				public int compare(Employee o1, Employee o2) {
					int s1 = Integer.parseInt(o1.getSalary());
					int s2 = Integer.parseInt(o2.getSalary());

					if (s1 > s2)
						return -1;
					if (s1 == s2)
						return 0;
					return 1;
				}
			};
		} else if(criteria.equals("ageAsc")) {
			comparator = new Comparator<Employee>() {
				@Override
				public int compare(Employee o1, Employee o2) {
					String s1 = o1.getCnp().substring(1, 7);
					String s2 = o2.getCnp().substring(1, 7);

					return s1.compareTo(s2);
				}
			};
		} else {
			throw new EmployeeException("invalid sorting criteria");
		}

		Collections.sort(employeeList, comparator);

		return employeeList;
	}

}
